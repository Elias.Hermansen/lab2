package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {

    List<FridgeItem> items = new ArrayList<>();
    int max_size = 20;

    @Override
    public int nItemsInFridge() {
        return this.items.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (this.totalSize() == this.nItemsInFridge()) {
            return false;
        } else {
            return this.items.add(item);
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (this.items.contains(item)) {
            this.items.remove(item);
        } else {
            throw new NoSuchElementException("This dosen't exist!");
        }
    }

    @Override
    public void emptyFridge() {
        this.items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for (int i = this.items.size()-1 ; i >= 0 ; i--) {
            if (this.items.get(i).hasExpired()) {
                expiredItems.add(this.items.get(i));
                this.items.remove(i);
            }
        }
        return expiredItems;
    }
   
}
